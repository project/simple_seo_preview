<?php

namespace Drupal\simple_seo_preview;

/**
 * Interface SimpleSeoPreviewManagerInterface.
 *
 * @package Drupal\simple_seo_preview
 */
interface SimpleSeoPreviewManagerInterface {

  /**
   * Generate elements array.
   *
   * @return array
   *   Elements.
   */
  public function generateElements();

}
