<?php

namespace Drupal\simple_seo_preview\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'simple_seo_preview_widget_type' widget.
 *
 * @FieldWidget(
 *   id = "simple_seo_preview_widget_type",
 *   label = @Translation("Simple SEO preview form"),
 *   field_types = {
 *     "simple_seo_preview"
 *   }
 * )
 */
class SimpleSeoPreviewWidgetType extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * AccountProxyInterface.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    AccountProxyInterface $current_user,
    Token $token,
    MessengerInterface $messenger) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->currentUser = $current_user;
    $this->tokenService = $token;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('token'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'seo_preview_expanded'  => TRUE,
      'title_size'            => 60,
      'title_max_char'        => 60,
      'description_rows'      => 3,
      'description_max_chars' => 155,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['seo_preview_expanded'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Default expand <em>Simple SEO preview</em> field ?'),
      '#default_value' => $this->getSetting('seo_preview_expanded'),
    ];
    $elements['title_size'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Title size'),
      '#default_value' => $this->getSetting('title_size'),
      '#required'      => TRUE,
      '#min'           => 1,
    ];
    $elements['title_max_char'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Title maximum characters'),
      '#default_value' => $this->getSetting('title_max_char'),
      '#required'      => TRUE,
      '#min'           => 1,
    ];
    $elements['description_rows'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Description rows'),
      '#default_value' => $this->getSetting('description_rows'),
      '#required'      => TRUE,
      '#min'           => 1,
    ];
    $elements['description_max_chars'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Description maximum characters'),
      '#default_value' => $this->getSetting('description_max_chars'),
      '#required'      => TRUE,
      '#min'           => 1,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $expanded_seo_preview_text = $this->t('No');
    $is_expanded_seo_preview = $this->getSetting('seo_preview_expanded');
    if (isset($is_expanded_seo_preview) && $is_expanded_seo_preview == TRUE) {
      $expanded_seo_preview_text = $this->t('Yes');
    }

    $summary[] = $this->t('Default expand <em>Simple SEO preview</em> field: @text', ['@text' => $expanded_seo_preview_text]);
    $summary[] = $this->t('Title size: @size', ['@size' => $this->getSetting('title_size')]);
    $summary[] = $this->t('Title max. chars: @size', ['@size' => $this->getSetting('title_max_char')]);
    $summary[] = $this->t('Description rows: @rows', ['@rows' => $this->getSetting('description_rows')]);
    $summary[] = $this->t('Description max. chars: @size', ['@size' => $this->getSetting('description_max_chars')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $form_state->setTemporaryValue('title_max_char', $this->getSetting('title_max_char'));
    $form_state->setTemporaryValue('description_max_chars', $this->getSetting('description_max_chars'));

    $node = $this->getFormNode($form_state);

    $default_title_value = '';
    $default_description_value = '';
    if (isset($node)) {
      /** @var \Drupal\node\Entity\NodeType $node_type */
      $node_type = $node->type->entity;
      $default_title_value = $node_type->getThirdPartySetting('simple_seo_preview', 'default_title_value');
      $default_description_value = $node_type->getThirdPartySetting('simple_seo_preview', 'default_description_value');
    }

    // Get field values.
    $item = $items[$delta];
    $values = [];
    if (!empty($item->value)) {
      $values = unserialize($item->value);
    }

    $original_title = isset($values['meta']['title']) && !empty($values['meta']['title']) ? $values['meta']['title'] : $default_title_value;
    $original_description = isset($values['meta']['description']) ? $values['meta']['description'] : $default_description_value;

    // Build preview values.
    list($title, $description, $url) = array_values($this->generateSnippetValues(
      $form_state,
      $original_title,
      $original_description
    ));

    // Add form validation.
    $element += [
      '#element_validate' => [[get_class($this), 'validateFormElement']],
    ];

    $element['value'] = [
      '#type'   => 'details',
      '#title'  => $element['#title'],
      '#open'   => $this->getSetting('seo_preview_expanded'),
      '#access' => $this->userHasViewPermission(),
    ];
    $element['value']['meta']['title'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Title'),
      '#required'      => isset($element['#required']) ? $element['#required'] : FALSE,
      '#default_value' => $original_title,
      '#size'          => $this->getSetting('title_size'),
      '#access'        => $this->userHasAdministerPermission(),
      '#description'   => $this->t('It is recommended that the title is no greater than @size characters long, including spaces.', [
        '@size' => $this->getSetting('title_max_char'),
      ]),
    ];
    $element['value']['meta']['description'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Description'),
      '#required'      => isset($element['#required']) ? $element['#required'] : FALSE,
      '#default_value' => $original_description,
      '#rows'          => $this->getSetting('description_rows'),
      '#access'        => $this->userHasAdministerPermission(),
      '#description'   => $this->t('It is recommended that the description is no greater than @size characters long, including spaces.', [
        '@size' => $this->getSetting('description_max_chars'),
      ]),
    ];

    // Token access link.
    $element['value']['tokens_tree_link'] = [
      '#prefix'      => '<p>',
      '#theme'       => 'token_tree_link',
      '#token_types' => [
        'node',
      ],
      '#access'      => $this->userHasAdministerPermission(),
      '#suffix'      => '</p>',
    ];

    $element['value']['overview'] = [
      '#type'   => 'fieldset',
      '#title'  => $this->t('Snippet'),
      '#access' => $this->userHasViewPermission(),
      'actions' => [
        'refresh' => [
          '#prefix'     => '<p>',
          '#type'       => 'submit',
          '#value'      => $this->t('Refresh preview below'),
          '#access'     => $this->userHasAdministerPermission(),
          '#attributes' => [
            'class' => ['button', 'button--primary'],
          ],
          '#ajax'       => [
            'callback'        => [$this, 'ajaxRefreshSnippet'],
            'disable-refocus' => TRUE,
            'wrapper'         => 'simple-seo-preview',
            'progress'        => [
              'type' => 'throbber',
            ],
          ],
          '#suffix'     => '</p>',
        ],
      ],
    ];

    $element['value']['overview']['snippet'] = $this->buildSnippetElement($title, $description, $url);

    // Add custom library.
    $element += [
      '#attached' => [
        'library' => [
          'simple_seo_preview/simple_seo_preview',
        ],
      ],
    ];

    return $element;
  }

  /**
   * Build snippet render array.
   *
   * @param string $title
   *   Title.
   * @param string $description
   *   Description.
   * @param string $url
   *   Url.
   *
   * @return array
   *   Render array.
   */
  public function buildSnippetElement($title, $description, $url) {
    $original_title = $title;
    $original_description = $description;

    $title = empty($title) ? $this->t('N/A') : $title;
    $description = empty($description) ? $this->t('N/A') : $description;

    $elements = [
      'title' => [
        '#type'       => 'html_tag',
        '#tag'        => 'div',
        '#value'      => $title,
        '#attributes' => [
          'class' => ['simple-seo-preview__title'],
        ],
      ],
      'url' => [
        '#type'       => 'html_tag',
        '#tag'        => 'div',
        '#value'      => $url,
        '#attributes' => [
          'class' => ['simple-seo-preview__url'],
        ],
      ],
      'description' => [
        '#type'       => 'html_tag',
        '#tag'        => 'div',
        '#value'      => $description,
        '#attributes' => [
          'class' => ['simple-seo-preview__description'],
        ],
      ],
    ];

    // Default display when title & description are empty.
    if (empty($original_title) && empty($original_description)) {
      $elements = [
        '#markup' => '<p><em>' . $this->t('No preview available: title and description are probably empty.') . '</em></p>',
      ];
    }

    return [
      '#type'       => 'html_tag',
      '#tag'        => 'div',
      '#value'      => render($elements),
      '#attributes' => [
        'id'    => 'simple-seo-preview',
        'class' => ['simple-seo-preview'],
      ],
    ];
  }

  /**
   * Refresh snippet render array.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState.
   *
   * @return array
   *   Render array.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function ajaxRefreshSnippet(array &$form, FormStateInterface $form_state) {
    $field_name = $this->fieldDefinition->getName();

    $field_seo_preview = $form_state->getValue($field_name);
    $values = reset($field_seo_preview);

    list($title, $description, $url) = array_values($this->generateSnippetValues(
      $form_state,
      $values['value']['meta']['title'],
      $values['value']['meta']['description'],
      TRUE
    ));

    // Delete all messages to prevent displaying main form errors.
    $this->messenger->deleteAll();

    return $this->buildSnippetElement($title, $description, $url);
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $flattened_value = [];
      foreach ($value as $group) {
        // Exclude the '_original_delta' value.
        if (is_array($group)) {
          foreach ($group as $key => $field_value) {
            $flattened_value[$key] = $field_value;
          }
        }
      }
      $value = serialize($flattened_value);
    }

    return $values;
  }

  /**
   * Form element validation handler for form element.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateFormElement(array &$element, FormStateInterface $form_state) {
    if ($element['#required'] == TRUE) {
      $title = '';
      $description = '';

      $values = $form_state->getValues();
      if (isset($values['field_seo_preview']) && !empty($values['field_seo_preview'])) {
        $title = $values['field_seo_preview'][0]['value']['meta']['title'];
        $description = $values['field_seo_preview'][0]['value']['meta']['description'];
      }

      $title_max_char = $form_state->getTemporaryValue('title_max_char');
      if (is_numeric($title_max_char) && strlen($title) > $title_max_char) {
        \Drupal::messenger()->addWarning(t('It is recommended that the title is no greater than @size characters long (currently @current_size chars), including spaces.', [
          '@size'         => $title_max_char,
          '@current_size' => strlen($title),
        ]));
      }

      $description_max_chars = $form_state->getTemporaryValue('description_max_chars');
      if (is_numeric($description_max_chars) && strlen($description) > $description_max_chars) {
        \Drupal::messenger()->addWarning(t('It is recommended that the description is no greater than @size characters long (currently @current_size chars), including spaces.', [
          '@size'         => $description_max_chars,
          '@current_size' => strlen($description),
        ]));
      }
    }
  }

  /**
   * Check if current user has view permission.
   *
   * @return bool
   *   TRUE if has permission, FALSE otherwise.
   */
  public function userHasViewPermission() {
    if ($this->userHasAdministerPermission()) {
      return TRUE;
    }

    return $this->currentUser->hasPermission('view simple_seo_preview meta tags preview');
  }

  /**
   * Check if current user has administer permission.
   *
   * @return bool
   *   TRUE if has permission, FALSE otherwise.
   */
  public function userHasAdministerPermission() {
    return $this->currentUser->hasPermission('administer simple_seo_preview meta tags');
  }

  /**
   * Get form node object.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormState.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Node object or NULL.
   */
  public function getFormNode(FormStateInterface $form_state) {
    $node = NULL;
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof ContentEntityForm) {
      $entity = $form_object->getEntity();
      if ($entity instanceof NodeInterface) {
        $node = $entity;
      }
    }

    return $node;
  }

  /**
   * Generate values before rendering.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormStateInterface.
   * @param string $title
   *   Title.
   * @param string $description
   *   Description.
   * @param boolean $ajax_call
   *   Boolean to check if it's a call ajax or not.
   *
   * @return array
   *   Values well formatted.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function generateSnippetValues(FormStateInterface $form_state, $title = '', $description = '', $ajax_call = FALSE) {
    $node = $this->getFormNode($form_state);

    // Default home URL.
    $url = Url::fromRoute('<front>', [], ['absolute' => TRUE, 'https' => TRUE])->toString();
    $url .= $this->t('<em>&lt;will be generated after node creation&gt;</em>');

    // Get node type default title & description values.
    if (isset($node)) {
      // Do not set default values when ajax call.
      if ($ajax_call === FALSE) {
        /** @var \Drupal\node\Entity\NodeType $node_type */
        $node_type = $node->type->entity;
        $default_title = $node_type->getThirdPartySetting('simple_seo_preview', 'default_title_value', '');
        if (empty($title)) {
          $title = $default_title;
        }
        $default_description = $node_type->getThirdPartySetting('simple_seo_preview', 'default_description_value', '');
        if (empty($description)) {
          $description = $default_description;
        }
      }

      // Generate title.
      if (!empty($title)) {
        $title = $this->tokenService->replace($title, [
          'node' => $node,
        ]);
        $title = strip_tags($title);
        $title_max_char = $form_state->getTemporaryValue('title_max_char');
        $title = Unicode::truncate($title, $title_max_char, FALSE, TRUE);
      }

      // Generate description.
      if (!empty($description)) {
        $description = $this->tokenService->replace($description, [
          'node' => $node,
        ]);
        $description = strip_tags($description);
        $description_max_chars = $form_state->getTemporaryValue('description_max_chars');
        $description = Unicode::truncate($description, $description_max_chars, FALSE, TRUE);
      }

      // Generate existing node URL.
      if (isset($node) && is_numeric($node->id())) {
        $url = $node->toUrl('canonical', ['absolute' => TRUE, 'https' => TRUE])->toString();
      }
    }

    return [
      'title'       => $title,
      'description' => $description,
      'url'         => $url,
    ];
  }

}
