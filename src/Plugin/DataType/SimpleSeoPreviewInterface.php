<?php

namespace Drupal\simple_seo_preview\Plugin\DataType;

use Drupal\Core\TypedData\PrimitiveInterface;

/**
 * The simple_seo_preview data type.
 *
 * The plain value of a simple_seo_preview is a serialized object represented
 * as a string.
 */
interface SimpleSeoPreviewInterface extends PrimitiveInterface {

}
