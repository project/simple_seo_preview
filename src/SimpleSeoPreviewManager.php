<?php

namespace Drupal\simple_seo_preview;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Utility\Token;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Class SimpleSeoPreviewManager.
 *
 * @package Drupal\simple_seo_preview
 */
class SimpleSeoPreviewManager implements SimpleSeoPreviewManagerInterface {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * SimpleSeoPreviewManager constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The RouteMatchInterface object.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct(
    RouteMatchInterface $route_match,
    Token $token) {
    $this->routeMatch = $route_match;
    $this->tokenService = $token;
  }

  /**
   * {@inheritdoc}
   */
  public function generateElements() {
    $elements = [];
    if ($node = $this->getNode()) {
      $tags = $this->tagsFromNode($node);
      foreach ($tags as $name => $content) {
        // Replace token here.
        $content = $this->tokenService->replace($content, [
          'node' => $node,
        ]);

        // Sanitize text removing all tags.
        $content = strip_tags($content);

        if (!empty($content)) {
          $this->generateElement($elements, 'meta', [
            'name'    => $name,
            'content' => $content,
          ]);
        }
      }
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function tagsFromNode(NodeInterface $node) {
    $tags = [];

    $fields = $this->getFields($node);

    /* @var \Drupal\field\Entity\FieldConfig $field_info */
    foreach ($fields as $field_name => $field_info) {
      // Get the tags from this field.
      $tags = $this->getFieldTags($node, $field_name);
    }
    return $tags;
  }

  /**
   * Returns a list of fields handled by Simple SEO preview.
   *
   * @return array
   *   A list of supported field types.
   */
  protected function fieldTypes() {
    return [
      'simple_seo_preview',
    ];
  }

  /**
   * Returns a list of the Simple SEO preview fields on an entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to examine.
   *
   * @return array
   *   The fields from the entity which are Simple SEO preview fields.
   */
  public function getFields(ContentEntityInterface $entity) {
    $field_list = [];

    if ($entity instanceof ContentEntityInterface) {
      // Get a list of the simple_seo_preview field types.
      $field_types = $this->fieldTypes();

      // Get a list of the field definitions on this entity.
      $definitions = $entity->getFieldDefinitions();

      // Iterate through all the fields looking for ones in our list.
      foreach ($definitions as $field_name => $definition) {
        // Get the field type, ie: simple_seo_preview.
        $field_type = $definition->getType();

        // Check the field type against our list of fields.
        if (isset($field_type) && in_array($field_type, $field_types)) {
          $field_list[$field_name] = $definition;
        }
      }
    }

    return $field_list;
  }

  /**
   * Get a node list of tags.
   *
   * @param \Drupal\node\NodeInterface $node
   *   NodeInterface object.
   * @param string $field_name
   *   Field machine name.
   *
   * @return array|mixed
   *   List of meta tags.
   */
  protected function getFieldTags(NodeInterface $node, $field_name) {
    $tags = [];
    foreach ($node->{$field_name} as $item) {
      // Get serialized value and break it into an array of tags with values.
      $serialized_values = $item->get('value')->getValue();
      if (!empty($serialized_values)) {
        $values = unserialize($serialized_values);
        if (isset($values['meta'])) {
          $tags = $values['meta'];
        }
      }
    }
    return $tags;
  }

  /**
   * Get current node.
   *
   * @return \Drupal\node\Entity\Node|null
   *   Route node.
   */
  public function getNode() {
    if ($this->routeMatch->getRouteName() === 'entity.node.canonical') {
      $node = $this->routeMatch->getParameter('node');
      if ($node instanceof Node) {
        return $node;
      }
    }
    return NULL;
  }

  /**
   * Generate attached element.
   *
   * @param array $elements
   *   Elements array.
   * @param string $tag
   *   Tag.
   * @param array $attributes
   *   Attributes.
   */
  public function generateElement(array &$elements, $tag, array $attributes) {
    $elements['#attached']['html_head'][] = [
      [
        '#tag'        => $tag,
        '#attributes' => $attributes,
      ],
      $attributes['name'],
    ];
  }

}
